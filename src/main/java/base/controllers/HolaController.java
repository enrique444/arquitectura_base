package base.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import base.models.User;
import base.services.UserService;

@RestController
public class HolaController {
	@Autowired
	private UserService service;
	
	@GetMapping(value = "/hola")
	ResponseEntity<String> greeting(){
		String greeting = service.getGreeting();
		return new ResponseEntity<String>(greeting, HttpStatus.OK);
	}
	
	@PostMapping(value = "/hola")
	ResponseEntity<String> personalGreeting(@RequestBody User body){
		User userReceived = body;
		String greeting = service.getPersonalGreeting(userReceived);
		return new ResponseEntity<String>(greeting, HttpStatus.OK);
	}

}
