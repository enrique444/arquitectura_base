package base.models;

public class User {
	private String name;
	
	private User() {}

	public static User createUser() {
		return new User();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	};
	
	
}
