package base.services;

import org.springframework.stereotype.Service;

import base.models.User;

@Service("UserService")
public class UserService {
	
	public String getGreeting() {
		return "Hola a todos!!";
	}
	
	public String getPersonalGreeting(User strange) {
		String greeting = "Hola " + strange.getName();
		return greeting;
	}
	
}
