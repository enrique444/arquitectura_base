package base.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import base.models.User;
import base.services.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest
public class HolaControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService userService;
	
	@Test
	public void greeting() throws Exception {
        when(userService.getGreeting()).thenReturn("Hola a todos!!");
        this.mockMvc.perform(get("/hola")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hola a todos!!")));
		
	}
	
	@Test
	public void getPersonalGreeting() throws Exception {
			User mockUser = User.createUser();
			mockUser.setName("Kike");
			
			Mockito.when(userService.getPersonalGreeting(mockUser))
				.thenReturn("Hola Kike");
			
			String exampleUserJson = "{\"name\":\"Kike\"}";
			
			this.mockMvc.perform(post("/hola")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(exampleUserJson)
			).andExpect(status().isOk());
	}
}
